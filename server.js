var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var requestjson = require('request-json');

var urlRaizMLab = "https://api.mlab.com/api/1/databases/crodriguezb/collections";

var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLabRaiz;
var ProductoMLabRaiz;


var urlUsuarios = "https://api.mlab.com/api/1/databases/crodriguezb/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlClientes = "https://api.mlab.com/api/1/databases/crodriguezb/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlProductos = "https://api.mlab.com/api/1/databases/crodriguezb/collections/Productos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlBanxico = "https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43718/datos/oportuno?token=9c02fe814e4d178cd60f2c573649bef2054943984ecc4c1400728988e4fcf4fc"


var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var movimientosJSON = require('./movimientosv2.json');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);


app.get('/Clientes', function(req, res){
  var clienteMLab = requestjson.createClient(urlClientes);
  clienteMLab.get('', function(err, resM, body) {
    if (err){
      console.log(body);
    } else {
      res.send(body);
    }
  })
})


app.get('/TipoCambio', function(req, res){
  var ProductoTC = requestjson.createClient(urlBanxico);
  ProductoTC.get('', function(err, resM, body) {
    if (err){
      console.log(body);
    } else {
      console.log(body);
      res.send(body);
    }
  })
})



app.get('/Productos', function(req, res){
  var ProductoMLab = requestjson.createClient(urlProductos);
  ProductoMLab.get('', function(err, resM, body) {
    if (err){
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

app.post('/Productos', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type");

  var index = req.body.index;
  var nombreProducto = req.body.nombreProducto;
  var montoMinimo = req.body.montoMinimo;
  var plazoMeses = req.body.plazoMeses;
  var tasa = req.body.Tasa;

  var data ={
    index:index,
    nombre_Producto:nombreProducto,
    monto_Minimo:montoMinimo,
    plazo_Meses:plazoMeses,
    Tasa:tasa
  };


  clienteMLabRaiz = requestjson.createClient(urlRaizMLab + "/Productos?" + apiKey);
  console.log(data);

  clienteMLabRaiz.post('', data, function(err, resM, body) {
    if(!err){
        res.status(200).send('Producto incluido');
        console.log('Producto incluido');
      } else {
        res.status(404).send('Productos NO incluido');
        console.log('Producto NO incluido');
      }
  })
})


app.post('/login', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var email = req.body.email
  var password = req.body.password
  var query = 'q={"email":"'+email+'","password":"'+password+'"}';

  clienteMLabRaiz = requestjson.createClient(urlRaizMLab + "/Usuarios?" + apiKey + "&" + query);
  console.log(urlRaizMLab + "/Usuarios?" + apiKey + "&" + query);

  clienteMLabRaiz.get('', function(err, resM, body) {
    if(!err){
      if(body.length == 1) { //Login ok
        res.status(200).send('Usuario logado');
      } else {
        res.status(404).send('Usuario no encontrado, registrese');
      }
    }
  })
})

app.post('/registra', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type");

  var email = req.body.email
  var password = req.body.password

  var data ={
    email:email,
    password:password
  };

  clienteMLabRaiz = requestjson.createClient(urlRaizMLab + "/Usuarios?" + apiKey);
  console.log(data);

  clienteMLabRaiz.post('', data, function(err, resM, body) {
    if(!err){
        res.status(200).send('Usuario Creado');
        console.log('Usuario Creado');
      } else {
        res.status(404).send('Usuario NO creado');
        console.log('Usuario NO creado');
      }
  })

})
